package synchronization.atomicVariables.sync;

import java.time.Duration;
import java.time.Instant;

public class ThreadsTest {
    static final int NUMBER_THREADS = 100;

    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();
        System.out.println("Initial Counter = " + counter.get());

        UpdateThread[] threads = new UpdateThread[NUMBER_THREADS];

        Instant start = Instant.now();
        for (int i = 0; i < NUMBER_THREADS; i++) {
            threads[i] = new UpdateThread(counter);
            threads[i].start();
        }

        for (int i = 0; i < NUMBER_THREADS; i++) {
            threads[i].join();
        }
        Instant stop = Instant.now();

        Long timeElapse = Duration.between(start, stop).toSeconds();
        System.out.println("Final Counter = " + counter.get());
        System.out.printf("Processed in: %d seconds", timeElapse);
    }
}

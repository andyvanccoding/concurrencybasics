package synchronization.staticmethod;

public class MyPrinter implements Runnable {

    @Override
    public void run() {

        PrintTasks print = new PrintTasks();
        while (true) {
            print.print();
            PrintTasks.printStatic();
            PrintTasks.printStatic2();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

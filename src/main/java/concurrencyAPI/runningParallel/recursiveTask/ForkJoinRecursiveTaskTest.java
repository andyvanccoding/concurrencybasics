package concurrencyAPI.runningParallel.recursiveTask;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinRecursiveTaskTest {
    static final int SIZE = 10_000_000;
    static int[] array = randomArray();

    private static int[] randomArray() {
        int[] array = new int[SIZE];
        Random rnd = new Random();

        for (int i = 0; i < SIZE; i++) {
            array[i] = rnd.nextInt(100);
        }
        return array;
    }

    public static void main(String[] args) {

        ArrayCounter mainTask = new ArrayCounter(array, 0, SIZE);
        ForkJoinPool pool = new ForkJoinPool();
        Integer evenNumberCount = pool.invoke(mainTask);
        System.out.println("Number of even numbers: " + evenNumberCount);


    }
}

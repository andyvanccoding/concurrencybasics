package concurrencyAPI.runningParallel.recursiveTask;

import java.util.concurrent.RecursiveTask;

/**
 * This class illustrates how to create a ForkJoinTask that returns a result.
 *
 * @author www.codejava.net
 * https://www.codejava.net/java-core/concurrency/understanding-java-fork-join-framework-with-examples
 */
public class ArrayCounter extends RecursiveTask<Integer> {

    int[] array;
    int threshold = 100_000;
    int start;
    int end;

    public ArrayCounter(int[] array, int start, int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Integer compute() {
        if (end - start < threshold) {
            return computeDirectly();
        } else {
            int middle = (end + start) / 2;

//          task is split in 2
            ArrayCounter subTask1 = new ArrayCounter(array, start, middle);
            ArrayCounter subTask2 = new ArrayCounter(array, middle, end);
//          tasks start
            invokeAll(subTask1, subTask2);
//          by using the join method, the result of the 2 tasks is combined
            return subTask1.join() + subTask1.join();
        }
    }

    private Integer computeDirectly() {
        Integer count = 0;

        for (int i = start; i < end; i++) {
            if (array[i] % 2 == 0) {
                count++;
            }
        }
        return count;
    }

}

package concurrencyAPI.runningParallel.recursiveAction;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

/**
 * This program demonstrates how to execute a result less ForkJoinTask in
 * a ForkJoinPool
 * @author www.codejava.net
 * https://www.codejava.net/java-core/concurrency/understanding-java-fork-join-framework-with-examples
 */
public class ForkJoinRecursiveActionTest {
    static final int SIZE = 10_000_000;
    static int[] array = randomArray();

    public static void main(String[] args) {

        int number = 9;

        System.out.println("First 10 elements of the array before: ");
        print();

        ArrayTransform mainTask = new ArrayTransform(array, number, 0, SIZE);
        ForkJoinPool pool = new ForkJoinPool();
        pool.invoke(mainTask);

        System.out.println("First 10 elements of the array after: ");
        print();
        System.out.println(array.length);
    }

    static int[] randomArray() {
        int[] array = new int[SIZE];
        Random random = new Random();

        for (int i = 0; i < SIZE; i++) {
            array[i] = random.nextInt(100);
        }

        return array;
    }

    static void print() {
        for (int i = 0; i < 10; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println();
    }
}

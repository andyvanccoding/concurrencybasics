package concurrencyAPI.threadPoolAndExecutors.singleThread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * MultipleTasksExecutorExample.java
 * This program demonstrates how to execute multiple tasks
 * with different kinds of executors.
 * @author www.codejava.net
 */
public class MultipleTasksExecutorExample {

    public static void main(String[] args) {

        ExecutorService pool = Executors.newSingleThreadExecutor();

        pool.execute(new CountDownClock("A"));
        pool.execute(new CountDownClock("B"));
        pool.execute(new CountDownClock("C"));
        pool.execute(new CountDownClock("D"));
        pool.execute(new CountDownClock("E"));
        pool.execute(new CountDownClock("F"));
        pool.execute(new CountDownClock("G"));

        pool.shutdown();

    }
}

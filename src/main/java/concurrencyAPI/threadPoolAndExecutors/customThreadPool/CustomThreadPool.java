package concurrencyAPI.threadPoolAndExecutors.customThreadPool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CustomThreadPool {
    int corePoolSize = 10;
    int maxPoolSize = 1000;
    int keepAliveTime = 120;
    BlockingQueue<Runnable> workQueue = new SynchronousQueue<Runnable>();

    ThreadPoolExecutor pool = new ThreadPoolExecutor(corePoolSize,
            maxPoolSize,
            keepAliveTime,
            TimeUnit.SECONDS,
            workQueue);

//    pool.execute(new RunnableTask());
}

package concurrencyAPI.threadPoolAndExecutors.customThreadPool;

import java.util.concurrent.*;

/**
 * MultipleTasksExecutorExample.java
 * This program demonstrates how to execute multiple tasks
 * with different kinds of executors.
 * @author www.codejava.net
 */
public class MultipleTasksExecutorExample {

    public static void main(String[] args) {

        int corePoolSize = 10;
        int maxPoolSize = 1000;
        int keepAliveTime = 120;
        BlockingQueue<Runnable> workQueue = new SynchronousQueue<Runnable>();

        ThreadPoolExecutor pool = new ThreadPoolExecutor(corePoolSize,
                maxPoolSize,
                keepAliveTime,
                TimeUnit.SECONDS,
                workQueue);

        pool.execute(new CountDownClock("A"));
        pool.execute(new CountDownClock("B"));
        pool.execute(new CountDownClock("C"));
        pool.execute(new CountDownClock("D"));
        pool.execute(new CountDownClock("E"));
        pool.execute(new CountDownClock("F"));
        pool.execute(new CountDownClock("G"));

        pool.shutdown();

    }
}

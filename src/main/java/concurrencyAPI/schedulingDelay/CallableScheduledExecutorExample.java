package concurrencyAPI.schedulingDelay;

import java.util.concurrent.*;

public class CallableScheduledExecutorExample {
    public static void main(String[] args) {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        Callable<Integer> task = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                //fake computation time
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
                return 1000_000;
            }
        };
        int delay = 5;
        Future<Integer> result = scheduler.schedule(task, delay, TimeUnit.SECONDS);
        try {
            Integer value = result.get();
            System.out.println("Value = " + value);
        }catch(InterruptedException | ExecutionException ex){
            ex.printStackTrace();
        }
        scheduler.shutdown();
    }
}

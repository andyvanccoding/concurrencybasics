package concurrencyAPI.multipleTasksConcurAtFixDel;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ConcurrentScheduledTasksExampleWithCancel {
    public static void main(String[] args) {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);

        CountDownClock clock1 = new CountDownClock("A");
        CountDownClock clock2 = new CountDownClock("B");
        CountDownClock clock3 = new CountDownClock("C");

        Future<?> f1 = scheduler.scheduleWithFixedDelay(clock1, 3, 10, TimeUnit.SECONDS);
        Future<?> f2 = scheduler.scheduleWithFixedDelay(clock2, 3, 15, TimeUnit.SECONDS);
        Future<?> f3 = scheduler.scheduleWithFixedDelay(clock3, 3, 20, TimeUnit.SECONDS);

        Runnable cancelTask = new Runnable() {
            public void run() {
                f1.cancel(true);
                f2.cancel(true);
                f3.cancel(true);
            }
        };

        scheduler.schedule(cancelTask, 20, TimeUnit.SECONDS);

    }
}

package fundamentals.threadexample;

public class ThreadInterrupt implements Runnable {

    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println("This is message #" + i);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException exception) {
//                System.out.println("I'm resumed");
                System.out.println("I'm about to stop");
                return; //thread will stop because the run method will return
            }
        }
    }
    public static void main(String[] args){
        Thread t1 = new Thread(new ThreadInterrupt());
        t1.start();
        try {
            Thread.sleep(5000);
            t1.interrupt();
        }catch (InterruptedException exception){
            //do nothing
        }
    }
}
